﻿using System;

#pragma warning disable SA1611

namespace QuickSort
{
    public static class Sorter
    {
        public static void QuickSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            if (array.Length <= 1)
            {
                return;
            }

            int startIndex = 0;
            int endIndex = array.Length - 1;
            int top = -1;
            int[] stack = new int[array.Length];

            stack[++top] = startIndex;
            stack[++top] = endIndex;

            while (top >= 0)
            {
                endIndex = stack[top--];
                startIndex = stack[top--];

                int pivot = Partition(ref array, startIndex, endIndex);

                if (pivot - 1 > startIndex)
                {
                    stack[++top] = startIndex;
                    stack[++top] = pivot - 1;
                }

                if (pivot + 1 < endIndex)
                {
                    stack[++top] = pivot + 1;
                    stack[++top] = endIndex;
                }
            }
        }

        public static void RecursiveQuickSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            if (array.Length <= 1)
            {
                return;
            }

            Sort(array, 0, array.Length - 1);
        }

        public static int Partition(ref int[] array, int minIndex, int maxIndex)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            int pivot = array[maxIndex];
            int lowIndex = minIndex - 1;

            for (int i = minIndex; i < maxIndex; i++)
            {
                if (array[i] <= pivot)
                {
                    lowIndex++;
                    Swap(ref array[lowIndex], ref array[i]);
                }
            }

            Swap(ref array[lowIndex + 1], ref array[maxIndex]);

            return lowIndex + 1;
        }

        public static void Swap(ref int leftItem, ref int rightItem)
        {
            int container = leftItem;
            leftItem = rightItem;
            rightItem = container;
        }

        public static void Sort(int[] array, int minIndex, int maxIndex)
        {
            if (minIndex < maxIndex)
            {
                int partitionIndex = Partition(ref array, minIndex, maxIndex);

                Sort(array, minIndex, partitionIndex - 1);
                Sort(array, partitionIndex + 1, maxIndex);
            }
        }
    }
}
